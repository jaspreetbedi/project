FROM adoptopenjdk/openjdk11:jdk-11.0.9_11-alpine
RUN apk add --no-cache tini=0.19.0-r0
ENTRYPOINT ["/sbin/tini", "--"]

COPY build/libs/b2b_v2.jar /app.jar
#ENV DEFAULT_JAVA_OPTIONS="-server -XX:+CMSClassUnloadingEnabled -XX:+HeapDumpOnOutOfMemoryError -XX:+UseGCOverheadLimit"
EXPOSE 8082
CMD ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar"]
